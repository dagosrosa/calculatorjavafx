package application;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) {
		launch(args);
		
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		try{AnchorPane root = FXMLLoader.load(getClass().getResource("/calculatorDesign.fxml"));
		primaryStage.getIcons().add(new Image("/icon.png"));
		primaryStage.setResizable(false);
		
		Scene scene = new Scene(root);
		scene.getStylesheets().add("style.css");
		primaryStage.setScene(scene);
		primaryStage.show();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
